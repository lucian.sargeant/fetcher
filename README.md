
# Fetcher

---

## About

Fetcher is a CLI tool used to fetch web pages given a list of urls.  

Once retrieved, the tool can display metadata about fetched sites.

---

## Installation


1. Create a virtual environment:
```bash
python3 -m venv venv
```
2. Activate the virtual environment:
```bash
source venv/bin/activate
```
3. Install dependencies:
```bash
python -m pip install -r requirements.txt
```

---

## How To Use

```bash
python fetch.py [OPTIONS] [URLS]...
```

>Options:  
--metadata:  Display metadata for fetched urls  
--help:      Shows help message and exits.

---

## Run With Docker

1. Build docker image:
```bash
docker build -t fetcher .
```
2. Run the docker image mounting the current working directory to the containers volume
```bash
docker run --rm -v $(pwd):/fetcher fetcher [OPTIONS] [URLS]...
```

---

## License
[MIT](https://choosealicense.com/licenses/mit/)
