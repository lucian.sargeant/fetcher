FROM python:3

WORKDIR /fetcher

ADD fetch.py ./
ADD requirements.txt ./

RUN python -m pip install -r requirements.txt

ENTRYPOINT [ "python", "./fetch.py" ]