import os
from datetime import datetime
from urllib.parse import urlparse

import click
import requests
import validators
from bs4 import BeautifulSoup


@click.command()
@click.argument('urls', nargs=-1)
@click.option('--metadata', is_flag=True, help='Display metadata for fetched urls')
def fetch(urls, metadata):
    for url in urls:

        try:
            if metadata:
                display_metadata(url)
            else:
                fetch_webpage(url)
        except Exception as e:
            click.echo(f'An unexpected error occurred: {e}')


def display_metadata(url):
    domain = extract_domain(url)
    file_name = f"{domain}.html"

    if not os.path.isfile(file_name):
        click.echo(f'No site data found for: {url}')
        return

    try:
        with open(file_name) as file:
            soup = BeautifulSoup(file, 'html.parser')

            click.echo(f'Site: {domain}')
            click.echo(f'Number of links: {count_links(soup)}')
            click.echo(f'Number of images: {count_images(soup)}')
            click.echo(f'Last fetched: {get_last_modified(file_name)}')

    except Exception as e:
        click.echo(f'Unable to read metadata: {e}')


def get_last_modified(file_name):
    return datetime.fromtimestamp(os.path.getmtime(file_name)).strftime('%a %b %d %Y %H:%M:%S')


def count_links(soup):
    return len(soup.find_all('a'))


def count_images(soup):
    return len(soup.find_all('img'))


def fetch_webpage(url):
    validation_result = validate_url(url)

    if validation_result is not True:
        click.echo(f'{url} is an invalid URL. Cannot retrieve site data.')
    else:
        click.echo(f'Fetching site data for {url}')

        request_site_data(url)


def validate_url(url):
    return validators.url(url.strip())


def request_site_data(url):
    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as e:
        click.echo(f'Something went wrong while retrieving site data: {e}')
    else:
        is_saved = save_webpage(extract_domain(url), response.text)

        if is_saved:
            click.echo('Successfully retrieved site data.')


def save_webpage(domain, html):
    parsed_html = BeautifulSoup(html, 'html.parser')

    try:
        with open(f'{domain}.html', 'w') as file:
            file.write(parsed_html.prettify())
    except IOError as e:
        click.echo(f'Unable to save site data: {e}')
        return False

    return True


def extract_domain(url):
    parse_result = urlparse(url)
    return parse_result.netloc


if __name__ == '__main__':
    fetch()
